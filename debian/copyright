Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: neomutt
Source: http://www.neomutt.org/
 The pristine upstream embeds a copy of jimtcl that is a generated bootstrap
 version of jimtcl. This was replaced with a build-dep on jimtcl.
Files-Excluded: autosetup/jimsh0.c

Files: *
Copyright: 1995      Ulrich Drepper <drepper@gnu.ai.mit.edu>
           1995-2008 Free Software Foundation, Inc.
           1996-2016 Michael R. Elkins <me@mutt.org>
           1996-2018 Brandon Long <blong@fiction.net>
           1998      Miroslav Vasko <vasko@ies.sk>
           1998-1999 Sang-Jin Hwang <accel@linux.mdworld.com>
           1998-2001 Andrej N. Gritsenko <andrej@lucky.net>
           1998-2006 Pawel Dziekonski <dzieko@pwr.wroc.pl>
           1998-2006 Sergiusz Pawłowicz
           1998-2008 Thomas Roessler <roessler@does-not-exist.org>
           1998-2019 Marc Baudoin <babafou@babafou.eu.org>
           1998-2023 Vincent Lefevre <vincent@vinc17.net>
           1998-2024 NeoMutt project
           1999      Michael Sobolev <mss@transas.com>
           1999-2001 Boris Wesslowski <Boris@Wesslowski.com>
           1999-2001 Boris Wesslowski <Boris@Wesslowski.com>
           1999-2001 Roberto Suarez Soto <ask4it@bigfoot.com>
           1999-2005 Fanis Dokianakis <madf@hellug.gr>
           1999-2007 Ronny Haryanto <ronny-neomutt-po-file@haryan.to>
           1999-2017 Brendan Cully <brendan@kublai.com>
           2000      Andrew W. Nosenko <awn@bcs.zp.ua>
           2000      Byeong-Chan Kim <redhands@linux.sarag.net>
           2000      Gediminas Paulauskas <menesis@delfi.lt>
           2000      Marco d'Itri <md@linux.it>
           2000      Marcus Brito <marcus@visaotec.com.br>
           2000      Tadas <btadas@is.lt>
           2000-2001 László Kiss <kissl@eptan.efe.hu>
           2000-2005 Byrial Jensen <byrial@image.dk>
           2000-2007 Edmund Grimley Evans <edmundo@rano.org>
           2000-2019 Morten Bo Johansen <mjb@mbjnet.dk>
           2000-2019 Vsevolod Volkov <vvv@mutt.org.ua>
           2000-2024 Marius Gedminas <marius@gedmin.as>
           2001      Anthony Wong <ypwong@debian.org>
           2001      Cd Chen <cdchen@mail.cynix.com.tw>
           2001      Fatih Demir <kabalak@gtranslator.org>
           2001      Mike Schiraldi <raldi@research.netsol.com>
           2001      Weichung Chau <weichung@mail.cynix.com.tw>
           2001-2002 Oliver Ehli <elmy@acm.org>
           2001-2003 Szabolcs Horváth <horvaths@fi.inf.elte.hu>
           2001-2016 Alexey Vyskubov <alexey@pepper.spb.ru>
           2001-2019 Ivan Vilata i Balaguer <ivan@selidor.net>
           2002      Toomas Soome <tsoome@muhv.pri.ee>
           2002-2004 Im Eunjea <eunjea@kldp.org>
           2002-2008 Roland Rosenfeld <roland@spinnaker.de>
           2002-2009 René Clerc <rene@clerc.nl>
           2002-2011 OOTA Toshiya <ribbon@users.sourceforge.net>
           2002-2019 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
           2003      Bjoern Jacke <bjoern@j3e.de>
           2003      Velko Hristov <hristov@informatik.hu-berlin.de>
           2004      Dan Ohnesorg <dan@ohnesorg.cz>
           2004      g10 Code GmbH
           2004-2007 Johan Svedberg <johan@svedberg.com>
           2004-2008 Piarres Beobide Egaña
           2004-2008 Piarres Beobide Egaña
           2005      Andreas Krennmair <ak@synflood.at>
           2005      Nikos Mayrogiannopoulos <nmav@hellug.gr>
           2005      Peter J. Holzer <hjp@hjp.net>
           2005      Simos Xenitellis <S.Xenitellis@rhbnc.ac.uk>
           2005      kromJx <kromJx@crosswinds.net>
           2005      ta_panta_rei <ta_panta_rei@flashmail.com>
           2005-2006 Kevin Patrick Scannell <scannell@slu.edu>
           2005-2009 Rocco Rutte <pdmef@gmx.net>
           2006      Recai Oktaş <roktas@debian.org>
           2007-2017 Petr Písař <petr.pisar@atlas.cz>
           2008-2017 Benno Schulenberg <benno@vertaalt.nl>
           2009-2016 Antonio Radici <antonio@dyne.org>
           2009-2020 Deng Xiyue <manphiz@gmail.com>
           2011-2019 Karel Zak <kzak@redhat.com>
           2012      Marco Paolone <marcopaolone@gmail.com>
           2013      Honza Horak
           2013      Maxim Krasilnikov <pseudo@avalon.org.ua>
           2015-2019 Kevin J. McCarthy <kevin@8t8.us>
           2015-2025 Richard Russon <rich@flatcap.org>
           2016      František Hájik <ferko.hajik@gmail.com>
           2016      Kevin Velghe <kevin@paretje.be>
           2016      Pierre-Elliott Bécue <becue@crans.org>
           2016      Rubén Llorente <porting@use.startmail.com>
           2016      Thomas Adam <thomas@xteddy.org>
           2016-2017 Bernard Pratz <guyzmo+github+pub@m0g.net>
           2016-2017 Damien Riegel <damien.riegel@gmail.com>
           2016-2018 André Berger <andre.berger@web.de>
           2016-2019 Ian Zimmerman <itz@no-use.mooo.com>
           2016-2025 Pietro Cerutti <gahr@gahr.ch>
           2017      Aleksa Sarai <cyphar@cyphar.com>
           2017      Anton Rieger <seishinryohosha@jikken.de>
           2017      Bryan Bennett <bbenne10@gmail.com>
           2017      Fabrice Bellet <fabrice@bellet.info>
           2017      Julian Andres Klode <jak@jak-linux.org>
           2017      Peter Lewis <pete@muddygoat.org>
           2017      Stefan Assmann <sassmann@kpanic.de>
           2017      Tobias Angele <toogley@mailbox.org>
           2017      William Pettersson <william.pettersson@gmail.com>
           2017      lilydjwg <lilydjwg@gmail.com>
           2017-2018 Reis Radomil
           2017-2019 Bo Yu <tsu.yubo@gmail.com>
           2017-2019 Mehdi Abaakouk <sileht@sileht.net>
           2017-2022 Austin Ray <austin@austinray.io>
           2018      Flammie Pirinen <flammie@iki.fi>
           2018      Floyd Anderson <f.a@31c0.net>
           2018      Gero Treuer <gero@70t.de>
           2018      Ivan J. <parazyd@dyne.org>
           2018      Perry Thompson <contact@ryper.org>
           2018-2019 David Sterba <dsterba@suse.cz>
           2018-2019 Simon Symeonidis <lethaljellybean@gmail.com>
           2018-2019 Thiago Costa de Paiva <tcpaiva@github>
           2018-2019 Victor Fernandes <criw@pm.me>
           2018-2020 Federico Kircheis <federico.kircheis@gmail.com>
           2018-2023 Marcin Rajner <marcin.rajner@pw.edu.pl>
           2018-2024 Jakub Jindra <jakub.jindra@socialbakers.com>
           2018-2024 Jakub Jindra <skomonster@gmail.com>
           2019      David Harrigan <dharrigan@gmail.com>
           2019      Fabian Groffen <grobian@gentoo.org>
           2019      Kevin Decherf <kevin@kdecherf.com>
           2019      Naveen Nathan <naveen@lastninja.net>
           2019      Sergey Alirzaev <zl29ah@gmail.com>
           2019-2020 Tino Reichardt <milky-neomutt@mcmilk.de>
           2019-2022 Zero King <l2dy@macports.org>
           2019-2023 Anna (navi) Figueiredo Gomes <navi@vlhl.dev>
           2020      Aditya De Saha <adityadesaha@gmail.com>
           2020      Adán Somoza <adansomoza@outlook.com>
           2020      Alexander Perlis
           2020      Lars Haalck <lars.haalck@uni-muenster.de>
           2020      Louis Brauer <louis@openbooking.ch>
           2020      Matthew Hughes <matthewhughes934@gmail.com>
           2020      R Primus <rprimus@gmail.com>
           2020      Romeu Vieira <romeu.bizz@gmail.com>
           2020      Thomas Sanchez <thomas.sanchz@gmail.com>
           2020      Valeri Sergeev <valsy@mail.ru>
           2020      Yousef Akbar <yousef@yhakbar.com>
           2020-2021 Reto Brunner <reto@slightlybroken.com>
           2020-2023 Roberto Alvarado <robdres123@gmail.com>
           2020-2023 наб <nabijaczleweli@nabijaczleweli.xyz>
           2020-2024 Emir SARI <emir_sari@icloud.com>
           2021      Allan Nordhøy <epost@anotheragency.no>
           2021      Ashish Panigrahi <ashish.panigrahi@protonmail.com>
           2021      Charalampos Kardaris <ckardaris@outlook.com>
           2021      Christian Ludwig <ludwig@ma.tum.de>
           2021      Christos Margiolis <christos@margiolis.net>
           2021      Eric Blake <eblake@redhat.com>
           2021      Ihor Antonov <ihor@antonovs.family>
           2021      Michael Constantine Dimopoulos <mk@mcdim.xyz>
           2021      Ryan Kavanagh <rak@rak.ac>
           2021      Thomas Bracht Laumann Jespersen <t@laumann.xyz>
           2021      Viktor Cheburkin <victor.cheburkin@gmail.com>
           2021      ftilde <ftilde@tamepointer.de>
           2021-2023 David Purton <dcpurton@marshwiggle.net>
           2021-2023 Gerrit Rüsing <gerrit@macclub-os.de>
           2021-2023 Страхиња Радић <contact@strahinja.org>
           2022      Carlos Henrique Lima Melara <charlesmelara@outlook.com>
           2022      Claes Nästén <pekdon@gmail.com>
           2022      Igor Serebryany <igor47@moomers.org>
           2022      Ilya Kurdyukov
           2022      Marco Sirabella <marco@sirabella.org>
           2022      Michal Siedlaczek <michal@siedlaczek.me>
           2022      Oliver Bandel <oliver@first.in-berlin.de>
           2022      Ramkumar Ramachandra <r@artagnon.com>
           2022      raf <raf@raf.org>
           2022-2023 Andrij Mizyk <andmizyk@gmail.com>
           2022-2023 Róbert Horváth <rob@nyar.eu>
           2023      Anna Figueiredo Gomes <navi@vlhl.dev>
           2023      Leon Philman
           2023      Rayford Shireman
           2023      Simon Reichel <simonreichel@giese-optik.de>
           2023      Steinar H Gunderson <steinar+neomutt@gunderson.no>
           2023-2024 Tóth János <gomba007@gmail.com>
           2023      Whitney Cumber
           2023-2024 Alejandro Colomar <alx@kernel.org>
           2023-2025 Dennis Schön <mail@dennis-schoen.de>
License: GPL-2+

Files: autosetup/*
Copyright: 2006-2016 WorkWare Systems <http://www.workware.net.au/>
License: BSD-2-Clause-Views

Files: autosetup/mutt-gettext.tcl
       autosetup/mutt-iconv.tcl
Copyright: 2017 Pietro Cerutti <gahr@gahr.ch>
License: BSD-2-clause

Files: data/colorschemes/zenburn.neomuttrc
Copyright: Renato Cunha
License: public-domain
 This is a zenburn-based neomutt color scheme that is not (even by far)
 complete. There's no copyright involved. Do whatever you want with it.
 Just be aware that I won't be held responsible if the current color-scheme
 explodes your mail client. ;)
 .
 This file is in the public domain.

Files: data/logo/neomutt.svg
Copyright: 2017 Malcolm Locke <malc@wholemeal.co.nz>
License: CC-BY-SA-4.0

Files: docs/mbox.5
Copyright: 2000 Thomas Roessler <roessler@does-not-exist.org>
License: public-domain
 This document is in the public domain and may be distributed and
 changed arbitrarily.

Files: mutt/queue.h
Copyright: 1991, 1993, The Regents of the University of California.
License: BSD-3-clause

Files: test-files/*
Copyright: Richard Russon <rich@flatcap.org>
License: GPL-2+

Files: pgpewrap.c
Copyright: Wessel Dankers <wsl@fruit.eu.org>
License: public-domain
 This code is in the public domain.

Files: po/ru.po
Copyright: 2021 Ivan Shmakov <ivan at siamics dot net>
License: CC0-1.0
Comment: copyright disclaimed per CC0

Files: test/acutest.h
Copyright: 2013-2020 Martin Mitas
           2019      Garrett D'Amore
License: Expat

Files: debian/*
Copyright: 2017-2023 Antonio Radici <antonio@debian.org>
           2019      Julian Andres Klode <jak@debian.org>
           2019-2020 Andreas Henriksson <andreas@fatal.se>
           2020      Jonathan Dowland <jmtd@debian.org>
           2020      Stefano Rivera <stefanor@debian.org>
           2021      Moritz Muehlenhoff <jmm@debian.org>
           2021      Ryan Kavanagh <rak@debian.org>
           2022      Timo Röhling <roehling@debian.org>
           2023      Pino Toscano <pino@debian.org>
           2023-2025 Carlos Henrique Lima Melara <charlesmelara@riseup.net>
License: GPL-2+

Files: debian/extra/lib/mailspell
Copyright: Brendan O'Dea <bod@debian.org>
License: public-domain
 By Brendan O'Dea <bod@debian.org>, public domain.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU General Public License can be
 found in "/usr/share/common-licenses/GPL-2".

License: BSD-2-Clause-Views
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials
    provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE WORKWARE SYSTEMS ``AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL WORKWARE
 SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 The views and conclusions contained in the software and documentation
 are those of the authors and should not be interpreted as representing
 official policies, either expressed or implied, of WorkWare Systems.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: CC0-1.0
 CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE LEGAL
 SERVICES. DISTRIBUTION OF THIS DOCUMENT DOES NOT CREATE AN ATTORNEY-CLIENT
 RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS INFORMATION ON AN "AS-IS" BASIS.
 CREATIVE COMMONS MAKES NO WARRANTIES REGARDING THE USE OF THIS DOCUMENT OR
 THE INFORMATION OR WORKS PROVIDED HEREUNDER, AND DISCLAIMS LIABILITY FOR
 DAMAGES RESULTING FROM THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS
 PROVIDED HEREUNDER.
 .
 Statement of Purpose
 .
 The laws of most jurisdictions throughout the world automatically confer
 exclusive Copyright and Related Rights (defined below) upon the creator and
 subsequent owner(s) (each and all, an "owner") of an original work of
 authorship and/or a database (each, a "Work").
 .
 Certain owners wish to permanently relinquish those rights to a Work for the
 purpose of contributing to a commons of creative, cultural and scientific
 works ("Commons") that the public can reliably and without fear of later
 claims of infringement build upon, modify, incorporate in other works, reuse
 and redistribute as freely as possible in any form whatsoever and for any
 purposes, including without limitation commercial purposes. These owners may
 contribute to the Commons to promote the ideal of a free culture and the
 further production of creative, cultural and scientific works, or to gain
 reputation or greater distribution for their Work in part through the use and
 efforts of others.
 .
 For these and/or other purposes and motivations, and without any expectation
 of additional consideration or compensation, the person associating CC0 with
 a Work (the "Affirmer"), to the extent that he or she is an owner of
 Copyright and Related Rights in the Work, voluntarily elects to apply CC0 to
 the Work and publicly distribute the Work under its terms, with knowledge of
 his or her Copyright and Related Rights in the Work and the meaning and
 intended legal effect of CC0 on those rights.
 .
 1. Copyright and Related Rights. A Work made available under CC0 may be
 protected by copyright and related or neighboring rights ("Copyright and
 Related Rights"). Copyright and Related Rights include, but are not limited
 to, the following:
 .
 i.   the right to reproduce, adapt, distribute, perform, display, communicate,
      and translate a Work;
 ii.  moral rights retained by the original author(s) and/or performer(s);
 iii. publicity and privacy rights pertaining to a person's image or likeness
      depicted in a Work;
 iv.  rights protecting against unfair competition in regards to a Work,
      subject to the limitations in paragraph 4(a), below;
 v.   rights protecting the extraction, dissemination, use and reuse of data
      in a Work;
 vi.  database rights (such as those arising under Directive 96/9/EC of the
      European Parliament and of the Council of 11 March 1996 on the legal
      protection of databases, and under any national implementation thereof,
      including any amended or successor version of such directive); and
 vii. other similar, equivalent or corresponding rights throughout the world
      based on applicable law or treaty, and any national implementations
      thereof.
 .
 2. Waiver. To the greatest extent permitted by, but not in contravention of,
 applicable law, Affirmer hereby overtly, fully, permanently, irrevocably and
 unconditionally waives, abandons, and surrenders all of Affirmer's Copyright
 and Related Rights and associated claims and causes of action, whether now
 known or unknown (including existing as well as future claims and causes of
 action), in the Work (i) in all territories worldwide, (ii) for the maximum
 duration provided by applicable law or treaty (including future time
 extensions), (iii) in any current or future medium and for any number of
 copies, and (iv) for any purpose whatsoever, including without limitation
 commercial, advertising or promotional purposes (the "Waiver"). Affirmer
 makes the Waiver for the benefit of each member of the public at large and to
 the detriment of Affirmer's heirs and successors, fully intending that such
 Waiver shall not be subject to revocation, rescission, cancellation,
 termination, or any other legal or equitable action to disrupt the quiet
 enjoyment of the Work by the public as contemplated by Affirmer's express
 Statement of Purpose.
 .
 3. Public License Fallback. Should any part of the Waiver for any reason be
 judged legally invalid or ineffective under applicable law, then the Waiver
 shall be preserved to the maximum extent permitted taking into account
 Affirmer's express Statement of Purpose. In addition, to the extent the
 Waiver is so judged Affirmer hereby grants to each affected person a
 royalty-free, non transferable, non sublicensable, non exclusive, irrevocable
 and unconditional license to exercise Affirmer's Copyright and Related Rights
 in the Work (i) in all territories worldwide, (ii) for the maximum duration
 provided by applicable law or treaty (including future time extensions),
 (iii) in any current or future medium and for any number of copies, and (iv)
 for any purpose whatsoever, including without limitation commercial,
 advertising or promotional purposes (the "License"). The License shall be
 deemed effective as of the date CC0 was applied by Affirmer to the Work.
 Should any part of the License for any reason be judged legally invalid or
 ineffective under applicable law, such partial invalidity or ineffectiveness
 shall not invalidate the remainder of the License, and in such case Affirmer
 hereby affirms that he or she will not (i) exercise any of his or her
 remaining Copyright and Related Rights in the Work or (ii) assert any
 associated claims and causes of action with respect to the Work, in either
 case contrary to Affirmer's express Statement of Purpose.
 .
 4. Limitations and Disclaimers.
 .
 a. No trademark or patent rights held by Affirmer are waived, abandoned,
    surrendered, licensed or otherwise affected by this document.
 b. Affirmer offers the Work as-is and makes no representations or warranties
    of any kind concerning the Work, express, implied, statutory or otherwise,
    including without limitation warranties of title, merchantability, fitness
    for a particular purpose, non infringement, or the absence of latent or
    other defects, accuracy, or the present or absence of errors, whether or
    not discoverable, all to the greatest extent permissible under applicable
    law.
 c. Affirmer disclaims responsibility for clearing rights of other persons
    that may apply to the Work or any use thereof, including without limitation
    any person's Copyright and Related Rights in the Work. Further, Affirmer
    disclaims responsibility for obtaining any necessary consents, permissions
    or other rights required for any use of the Work.
 d. Affirmer understands and acknowledges that Creative Commons is not a
    party to this document and has no duty or obligation with respect to this
    CC0 or use of the Work.

License: CC-BY-SA-4.0
 Creative Commons Attribution-ShareAlike 4.0 International Public License
 .
 By exercising the Licensed Rights (defined below), You accept and agree
 to be bound by the terms and conditions of this Creative Commons
 Attribution-ShareAlike 4.0 International Public License ("Public
 License"). To the extent this Public License may be interpreted as a
 contract, You are granted the Licensed Rights in consideration of Your
 acceptance of these terms and conditions, and the Licensor grants You
 such rights in consideration of benefits the Licensor receives from
 making the Licensed Material available under these terms and conditions.
 .
 Section 1 – Definitions.
 .
 Adapted Material means material subject to Copyright and Similar Rights
 that is derived from or based upon the Licensed Material and in which
 the Licensed Material is translated, altered, arranged, transformed, or
 otherwise modified in a manner requiring permission under the Copyright
 and Similar Rights held by the Licensor. For purposes of this Public
 License, where the Licensed Material is a musical work, performance, or
 sound recording, Adapted Material is always produced where the Licensed
 Material is synched in timed relation with a moving image.  Adapter's
 License means the license You apply to Your Copyright and Similar Rights
 in Your contributions to Adapted Material in accordance with the terms
 and conditions of this Public License.
 BY-SA Compatible License means a license listed at
 creativecommons.org/compatiblelicenses, approved by Creative Commons as
 essentially the equivalent of this Public License.  Copyright and
 Similar Rights means copyright and/or similar rights closely related to
 copyright including, without limitation, performance, broadcast, sound
 recording, and Sui Generis Database Rights, without regard to how the
 rights are labeled or categorized. For purposes of this Public License,
 the rights specified in Section 2(b)(1)-(2) are not Copyright and
 Similar Rights.
 Effective Technological Measures means those measures that, in the
 absence of proper authority, may not be circumvented under laws
 fulfilling obligations under Article 11 of the WIPO Copyright Treaty
 adopted on December 20, 1996, and/or similar international agreements.
 Exceptions and Limitations means fair use, fair dealing, and/or any
 other exception or limitation to Copyright and Similar Rights that
 applies to Your use of the Licensed Material.
 License Elements means the license attributes listed in the name of a
 Creative Commons Public License. The License Elements of this Public
 License are Attribution and ShareAlike.  Licensed Material means the
 artistic or literary work, database, or other material to which the
 Licensor applied this Public License.
 Licensed Rights means the rights granted to You subject to the terms
 and conditions of this Public License, which are limited to all
 Copyright and Similar Rights that apply to Your use of the Licensed
 Material and that the Licensor has authority to license.  Licensor means
 the individual(s) or entity(ies) granting rights under this Public
 License.
 Share means to provide material to the public by any means or process
 that requires permission under the Licensed Rights, such as
 reproduction, public display, public performance, distribution,
 dissemination, communication, or importation, and to make material
 available to the public including in ways that members of the public may
 access the material from a place and at a time individually chosen by
 them.  Sui Generis Database Rights means rights other than copyright
 resulting from Directive 96/9/EC of the European Parliament and of the
 Council of 11 March 1996 on the legal protection of databases, as
 amended and/or succeeded, as well as other essentially equivalent rights
 anywhere in the world.
 You means the individual or entity exercising the Licensed Rights under
 this Public License. Your has a corresponding meaning.
 .
 Section 2 – Scope.
 .
 License grant.
 Subject to the terms and conditions of this Public License, the
 Licensor hereby grants You a worldwide, royalty-free, non-sublicensable,
 non-exclusive, irrevocable license to exercise the Licensed Rights in
 the Licensed Material to: reproduce and Share the Licensed Material, in
 whole or in part; and
 produce, reproduce, and Share Adapted Material.
 Exceptions and Limitations. For the avoidance of doubt, where
 Exceptions and Limitations apply to Your use, this Public License does
 not apply, and You do not need to comply with its terms and conditions.
 Term. The term of this Public License is specified in Section 6(a).
 Media and formats; technical modifications allowed. The Licensor
 authorizes You to exercise the Licensed Rights in all media and formats
 whether now known or hereafter created, and to make technical
 modifications necessary to do so. The Licensor waives and/or agrees not
 to assert any right or authority to forbid You from making technical
 modifications necessary to exercise the Licensed Rights, including
 technical modifications necessary to circumvent Effective Technological
 Measures. For purposes of this Public License, simply making
 modifications authorized by this Section 2(a)(4) never produces Adapted
 Material.  Downstream recipients.
 Offer from the Licensor – Licensed Material. Every recipient of the
 Licensed Material automatically receives an offer from the Licensor to
 exercise the Licensed Rights under the terms and conditions of this
 Public License.  Additional offer from the Licensor – Adapted Material.
 Every recipient of Adapted Material from You automatically receives an
 offer from the Licensor to exercise the Licensed Rights in the Adapted
 Material under the conditions of the Adapter’s License You apply.
 No downstream restrictions. You may not offer or impose any additional
 or different terms or conditions on, or apply any Effective
 Technological Measures to, the Licensed Material if doing so restricts
 exercise of the Licensed Rights by any recipient of the Licensed
 Material.  No endorsement. Nothing in this Public License constitutes or
 may be construed as permission to assert or imply that You are, or that
 Your use of the Licensed Material is, connected with, or sponsored,
 endorsed, or granted official status by, the Licensor or others
 designated to receive attribution as provided in Section 3(a)(1)(A)(i).
 Other rights.
 .
 Moral rights, such as the right of integrity, are not licensed under
 this Public License, nor are publicity, privacy, and/or other similar
 personality rights; however, to the extent possible, the Licensor waives
 and/or agrees not to assert any such rights held by the Licensor to the
 limited extent necessary to allow You to exercise the Licensed Rights,
 but not otherwise.  Patent and trademark rights are not licensed under
 this Public License.
 To the extent possible, the Licensor waives any right to collect
 royalties from You for the exercise of the Licensed Rights, whether
 directly or through a collecting society under any voluntary or waivable
 statutory or compulsory licensing scheme. In all other cases the
 Licensor expressly reserves any right to collect such royalties.
 .
 Section 3 – License Conditions.
 .
 Your exercise of the Licensed Rights is expressly made subject to the
 following conditions.
 .
 Attribution.
 .
 If You Share the Licensed Material (including in modified form), You must:
 .
 retain the following if it is supplied by the Licensor with the
 Licensed Material: identification of the creator(s) of the Licensed
 Material and any others designated to receive attribution, in any
 reasonable manner requested by the Licensor (including by pseudonym if
 designated);
 a copyright notice;
 a notice that refers to this Public License;
 a notice that refers to the disclaimer of warranties;
 a URI or hyperlink to the Licensed Material to the extent reasonably
 practicable; indicate if You modified the Licensed Material and retain
 an indication of any previous modifications; and
 indicate the Licensed Material is licensed under this Public License,
 and include the text of, or the URI or hyperlink to, this Public
 License.  You may satisfy the conditions in Section 3(a)(1) in any
 reasonable manner based on the medium, means, and context in which You
 Share the Licensed Material. For example, it may be reasonable to
 satisfy the conditions by providing a URI or hyperlink to a resource
 that includes the required information.
 If requested by the Licensor, You must remove any of the information
 required by Section 3(a)(1)(A) to the extent reasonably practicable.
 ShareAlike.
 In addition to the conditions in Section 3(a), if You Share Adapted
 Material You produce, the following conditions also apply.
 .
 The Adapter’s License You apply must be a Creative Commons license with
 the same License Elements, this version or later, or a BY-SA Compatible
 License.  You must include the text of, or the URI or hyperlink to, the
 Adapter's License You apply. You may satisfy this condition in any
 reasonable manner based on the medium, means, and context in which You
 Share Adapted Material.
 You may not offer or impose any additional or different terms or
 conditions on, or apply any Effective Technological Measures to, Adapted
 Material that restrict exercise of the rights granted under the
 Adapter's License You apply.
 .
 Section 4 – Sui Generis Database Rights.
 .
 Where the Licensed Rights include Sui Generis Database Rights that
 apply to Your use of the Licensed Material:
 .
 for the avoidance of doubt, Section 2(a)(1) grants You the right to
 extract, reuse, reproduce, and Share all or a substantial portion of the
 contents of the database; if You include all or a substantial portion of
 the database contents in a database in which You have Sui Generis
 Database Rights, then the database in which You have Sui Generis
 Database Rights (but not its individual contents) is Adapted Material,
 including for purposes of Section 3(b); and
 You must comply with the conditions in Section 3(a) if You Share all or
 a substantial portion of the contents of the database.  For the
 avoidance of doubt, this Section 4 supplements and does not replace Your
 obligations under this Public License where the Licensed Rights include
 other Copyright and Similar Rights.
 .
 Section 5 – Disclaimer of Warranties and Limitation of Liability.
 .
 Unless otherwise separately undertaken by the Licensor, to the extent
 possible, the Licensor offers the Licensed Material as-is and
 as-available, and makes no representations or warranties of any kind
 concerning the Licensed Material, whether express, implied, statutory,
 or other. This includes, without limitation, warranties of title,
 merchantability, fitness for a particular purpose, non-infringement,
 absence of latent or other defects, accuracy, or the presence or absence
 of errors, whether or not known or discoverable. Where disclaimers of
 warranties are not allowed in full or in part, this disclaimer may not
 apply to You.  To the extent possible, in no event will the Licensor be
 liable to You on any legal theory (including, without limitation,
 negligence) or otherwise for any direct, special, indirect, incidental,
 consequential, punitive, exemplary, or other losses, costs, expenses, or
 damages arising out of this Public License or use of the Licensed
 Material, even if the Licensor has been advised of the possibility of
 such losses, costs, expenses, or damages. Where a limitation of
 liability is not allowed in full or in part, this limitation may not
 apply to You.
 The disclaimer of warranties and limitation of liability provided above
 shall be interpreted in a manner that, to the extent possible, most
 closely approximates an absolute disclaimer and waiver of all liability.
 .
 Section 6 – Term and Termination.
 .
 This Public License applies for the term of the Copyright and Similar
 Rights licensed here. However, if You fail to comply with this Public
 License, then Your rights under this Public License terminate
 automatically.  Where Your right to use the Licensed Material has
 terminated under Section 6(a), it reinstates:
 .
 automatically as of the date the violation is cured, provided it is
 cured within 30 days of Your discovery of the violation; or upon express
 reinstatement by the Licensor.
 For the avoidance of doubt, this Section 6(b) does not affect any right
 the Licensor may have to seek remedies for Your violations of this
 Public License.  For the avoidance of doubt, the Licensor may also offer
 the Licensed Material under separate terms or conditions or stop
 distributing the Licensed Material at any time; however, doing so will
 not terminate this Public License.
 Sections 1, 5, 6, 7, and 8 survive termination of this Public License.
 .
 Section 7 – Other Terms and Conditions.
 .
 The Licensor shall not be bound by any additional or different terms or
 conditions communicated by You unless expressly agreed.  Any
 arrangements, understandings, or agreements regarding the Licensed
 Material not stated herein are separate from and independent of the
 terms and conditions of this Public License.
 .
 Section 8 – Interpretation.
 .
 For the avoidance of doubt, this Public License does not, and shall not
 be interpreted to, reduce, limit, restrict, or impose conditions on any
 use of the Licensed Material that could lawfully be made without
 permission under this Public License.  To the extent possible, if any
 provision of this Public License is deemed unenforceable, it shall be
 automatically reformed to the minimum extent necessary to make it
 enforceable. If the provision cannot be reformed, it shall be severed
 from this Public License without affecting the enforceability of the
 remaining terms and conditions.
 No term or condition of this Public License will be waived and no
 failure to comply consented to unless expressly agreed to by the
 Licensor.  Nothing in this Public License constitutes or may be
 interpreted as a limitation upon, or waiver of, any privileges and
 immunities that apply to the Licensor or You, including from the legal
 processes of any jurisdiction or authority.
